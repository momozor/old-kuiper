extends TextureFrame

func _on_startNewGame_pressed():
	get_tree().change_scene("res://scenes/game/intro/Intro.tscn")

func _on_settingsMenu_pressed():
	get_tree().change_scene("res://scenes/misc/settings/Settings.tscn")

func _on_credits_pressed():
	get_tree().change_scene("res://scenes/misc/credits/CreditsMenu.tscn")

# quit the entire game
func _on_exit_pressed():
	get_tree().quit()

func _ready():
	get_node("startNewGame").connect("pressed", self, "_on_startNewGame_pressed")
	get_node("settingsMenu").connect("pressed", self, "_on_settingsMenu_pressed")
	get_node("credits").connect("pressed", self, "_on_credits_pressed")
	get_node("exit").connect("pressed", self, "_on_exit_pressed")
