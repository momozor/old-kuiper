extends TextureFrame

# open the system web browser to project home page
# for reference
func _on_projectHome_pressed():
	OS.shell_open("https://github.com/momozor/beyond-kuiper")

func _on_back_pressed():
	get_tree().change_scene("res://scenes/misc/MainMenu.tscn")

func _ready():
	get_node("projectHome").connect("pressed", self, "_on_projectHome_pressed")
	get_node("back").connect("pressed", self, "_on_back_pressed")
