extends TextureFrame

# turn whole game music on/off
func _on_musicTurn_pressed():
	
	# turn off music
	if (get_node("musicTurn").get_text() == "Music: On"):
		AudioServer.set_stream_global_volume_scale(0)
		get_node("musicTurn").set_text("Music: Off")
		
	# turn on music
	elif (get_node("musicTurn").get_text() == "Music: Off"):
		AudioServer.set_stream_global_volume_scale(1)
		get_node("musicTurn").set_text("Music: On")
		
func _on_backToMainMenu_pressed():
	get_tree().change_scene("res://scenes/misc/MainMenu.tscn")

# credits menu functions
# open a web browser to project 
#func _on_projectHome_pressed():
	#OS.shell_open("https://github.com/momozor/beyond-kuiper")

func _ready():
	if (AudioServer.get_stream_global_volume_scale() == 1):
		get_node("musicTurn").set_text("Music: On")
		
	elif (AudioServer.get_stream_global_volume_scale() == 0):
		get_node("musicTurn").set_text("Music: Off")
	
	get_node("musicTurn").connect("pressed", self, "_on_musicTurn_pressed") 
	get_node("backToMainMenu").connect("pressed", self, "_on_backToMainMenu_pressed")
	
	#get_node("projectHome").connect("pressed", self, "_on_projectHome_pressed")
