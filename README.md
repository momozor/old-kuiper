# Beyond Kuiper
What else is out there?

![mm](https://user-images.githubusercontent.com/24475030/50744246-4e368d80-125c-11e9-8f5d-1a4fbde80ec3.png)

Beyond Kuiper is a fully randomized text of vast space adventure game.

It aims to be *different*, everytime you play it. Most games are generally
repetitive, tedious and predictable to play. Unlike those kind of games,
it aims to be a *true exploration* platform where whatever you do, your action
will affect your next journey in the game, but unlike other games, your
next wander isn't limited to a tiny set of predefined stories.

## Why text interface and English?
Text is one of the most ancient medium we are still using to communicate
ideas and knowledge, even today and this text you are reading as the prime
example.

By using text, all of us can imagine the situation based on our experience
of the past and that will be vague, which is the game is also want to be.
Imagination is suprisingly ambiguous and to imagine something for everyone
will almost always be distinguishable.

There are hundreds of human languages and millions of their speakers, and
the game is prefering to use English due to ubiquitous status of the language.
While it doesn't have the most native speakers in the world compared to
Mandarin, the speakers (both native and non-native) can be found in almost
every section of the world. This will be beneficial for anybody who
understand English and want to use the generated adventure of the game
as a topic or story to those who doesn't.

## So what Kuiper has anything to do with this game?
*Kuiper Belt* is an outer line of our solar system. We had discovered 
many things and we are learning more as we go to understand our neighbouring
planets, the giant ball of burning oxygen, moons and even ourselves. But
outside of our solar system or in this case beyond the Kuiper Belt, we
have so many things that we are yet to discover. This game is not all about
*just* exploring the space, but to wonder about the world.
What else is out there? What are we looking for? Why everything is the way
it is?

Do we dare to question the unknown?

## Features

* This game *will* include many philosophical texts and scientific talks.
    - Two of the sources will be from Alan Watts and Carl Sagan.
    - Big Bang
    - Darwinism
    - Zen
    - Humanity
    - Survival
    - Culture

* This game is 2D text space adventure game. 
    - It should run on GNU/Linux-based kernel operating system, Windows and 
    MacOSX proprietary systems.
    - It may run on Android, iOS and Windows phone mobile operating
    systems.
    - It may run in the web browser and even in a command line program.
    - It should require very low resources to run.
    - Open, wide space exploration is the main theme of the game.

* It is *fully* randomized.
    - If possible, generate millions of different stories, planets,
    civilizations, creatures, planets algorithmically
    if not billions using No Man's Sky video game as a reference.

* It should be free of ads, free in price and free as in freedom of speech.
    - The game should not contain any commercial advertisements or links
    to any particular for-profit party to promote their causes or products.

    - It should totally be free as in price. $0 to play, install 
    and run this game.

    - You (user or player) should be able to use, read, distribute and modify
    the source code of this game without fear of 'breaking' the law.
    As long as it is compatible with GNU GPLv3.0 license.

## Authors
* momozor (Momo)

## License
This project is licensed under the GPL-3.0 license. See LICENSE file for
further details.
